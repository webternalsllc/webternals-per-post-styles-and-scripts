<?php
    /**
     * Plugin Name: Per-Post Style and Scripts
     * Plugin URI: https://bitbucket.org/webternalsllc/webternals-per-post-style-and-scripts
     * Description: Allows user to add custom Styles and Scripts to a post using the post edit page.
     * Version: 1.0.1
     * Author: Webternals, LLC (R. Allen Sanford)
     * Author URI: http://www.webternals.com
     * License: GPLv2 or later
     *
     * */

    defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

    /**
     * Allows user to add custom Styles and Scripts to a post using the post edit page.
     */
    class WebternalsPerPostStylesAndScripts
    {
        /**
         * The singelton instance
         * @var object
         */
        private static $instance;

        /**
         * Setup the plungin hooks, filters, yaddie yaddie, yaddie.
         */
        public function __construct( )
        {
            add_filter( 'admin_menu', array( $this, 'add_metaboxes' ) );
            add_filter( 'save_post', array( $this, 'save_post' ) );
            add_filter( 'wp_footer', array( $this, 'inject_inline_script' ) );
            add_filter( 'wp_head', array( $this, 'inject_inline_style' ) );
        }

        /**
         * Singleton instance method.
         * @return Controller The instance
         */
        public static function __instance()
        {
            if ( !isset( self::$instance ) )
            {
                self::$instance = new self(); //PHP 5.3.0 or so we can use static
            }

            return self::$instance;
        }

        /**
         * Add the metaboxes to the post edit page.
         */
        public function add_metaboxes( )
        {
            foreach ( ( array ) get_post_types( array( 'public' => true ) ) as $type )
            {
                add_meta_box( 'webternals_inline_style', 'Custom Inline CSS', array( $this, 'insert_inline_style_textarea' ), $type, 'normal', 'low' );
                add_meta_box( 'webternals_inline_script', 'Custom Inline JavaScript', array( $this, 'insert_inline_script_textarea' ), $type, 'normal', 'low' );
            }
        }

        /**
         * Display the inline script contents to the page
         */
        public function inject_inline_script()
        {
            $inline_script = get_post_meta( get_the_ID(), '_webternals_inline_script', true );
            if ( $inline_script )
            {
                echo "<script type=\"text/javascript\">{$inline_script}</script>";
            }
        }

        /**
         * Display the inline styles content to the page.
         */
        public function inject_inline_style()
        {
            $inline_style = get_post_meta( get_the_ID(), '_webternals_inline_style', true );
            if ( $inline_style )
            {
                echo "<style type=\"text/css\">{$inline_style}</style>";
            }
        }

        /**
         * Will create our textarea for our inline script metabox.
         */
        public function insert_inline_script_textarea()
        {

            $inline_script = esc_textarea( get_post_meta( get_the_ID(), '_webternals_inline_script', true ) );

            wp_nonce_field( basename( __FILE__ ), 'webternals_inline_script_nonce' );

            echo "<textarea name=\"webternals_inline_script\" id=\"webternals_inline_script\" rows=\"10\" style=\"width:100%;\">{$inline_script}</textarea>";
        }

        /**
         * Will create our textarea for our inline style metabox.
         */
        public function insert_inline_style_textarea()
        {

            $inline_style    = get_post_meta( get_the_ID(), '_webternals_inline_style', true );

            wp_nonce_field( basename( __FILE__ ), 'webternals_inline_style_nonce' );

            echo "<textarea name=\"webternals_inline_style\" id=\"webternals_inline_style\" rows=\"10\" style=\"width:100%;\">{$inline_style}</textarea>";
        }

        /**
         * Saves our inline script content for a given page.
         * @param int $post_id
         * @return mixed
         */
        public function save_inline_script_content( $post_id )
        {

            if ( !isset( $_POST[ 'webternals_inline_script_nonce' ] ) || !wp_verify_nonce( $_POST[ 'webternals_inline_script_nonce' ], basename( __FILE__ ) ) )
            {
                return $post_id;
            }

            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            {
                return $post_id;
            }

            $new_content = ( isset( $_POST[ 'webternals_inline_script' ] ) ? $_POST[ 'webternals_inline_script' ] : '' );
            $content     = get_post_meta( $post_id, '_webternals_inline_script', true );

            if ( $new_content && $new_content != $content )
            {
                update_post_meta( $post_id, '_webternals_inline_script', $new_content );
            }
            else if ( '' == $new_content && $content )
            {
                delete_post_meta( $post_id, '_webternals_inline_script', $content );
            }
        }

        /**
         * Saves our inline style content for a given page.
         * @param int $post_id
         * @return mixed
         */
        public function save_inline_style_content( $post_id )
        {

            if ( !isset( $_POST[ 'webternals_inline_style_nonce' ] ) || !wp_verify_nonce( $_POST[ 'webternals_inline_style_nonce' ], basename( __FILE__ ) ) )
            {
                return $post_id;
            }

            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            {
                return $post_id;
            }

            $new_content = ( isset( $_POST[ 'webternals_inline_style' ] ) ? esc_textarea( $_POST[ 'webternals_inline_style' ] ) : '' );
            $content     = get_post_meta( $post_id, '_webternals_inline_style', true );

            if ( $new_content && $new_content != $content )
            {
                update_post_meta( $post_id, '_webternals_inline_style', $new_content );
            }
            else if ( '' == $new_content && $content )
            {
                delete_post_meta( $post_id, '_webternals_inline_style', $content );
            }
        }

        /**
         * Save the content of the post for this plugin.
         * @param type $post_id
         */
        public function save_post( $post_id )
        {
            $this->save_inline_script_content( $post_id );
            $this->save_inline_style_content( $post_id );
        }
    }

    // Initiate the plugin
    WebternalsPerPostStylesAndScripts::__instance();