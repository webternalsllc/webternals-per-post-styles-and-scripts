=== Plugin Name ===
Contributors: 166873
Donate link: http://www.webternals.com/
Tags: script, style, inline, css, javascript
Requires at least: 3.0.1
Tested up to: 4.3
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows user to add custom Styles and Scripts to a post using the post edit page.

== Description ==

Allows user to add custom Styles and Scripts to a post using the post edit page.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `webternals=per-post-styles-and-scripts` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place code in the meta boxes on the post edit page.

== Frequently Asked Questions ==

= Why is there no F.A.Qs in the Frequently Asked Questions Section? =

Simple, no one has asked a question yet!

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).

== Changelog ==

= 1.0 =
* Plugin Created

== Upgrade Notice ==

= 1.0 =
Uh, because the plugin is just now being created!
